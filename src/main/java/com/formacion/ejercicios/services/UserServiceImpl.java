package com.formacion.ejercicios.services;

import com.formacion.ejercicios.dao.UserDao;
import com.formacion.ejercicios.dao.UserDaoImpl;
import com.formacion.ejercicios.dto.UserDto;

public class UserServiceImpl implements UserService{

	//Atributo
	private UserDao userDao;
	
	//Constructor
	public UserServiceImpl(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@Override
	public UserDto getUserById(long id) {
		
		//invoco un método de UserDao para obtener los datos del usuario en la base de datos
		UserDto user = userDao.getUserById(id);
		
		if("M".equals(user.getGender())) {
			user.setGender("MASCULINO");
		}else {
			user.setGender("FEMENINO");
		}
		
		
		return user;
	}
	

}
