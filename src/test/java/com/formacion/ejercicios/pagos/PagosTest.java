package com.formacion.ejercicios.pagos;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PagosTest {
	
	
	@Test
	public void validarPagos() {
		Pagos p = new Pagos();
		
		int resultado =  p.sumaPagos(10, 10, 10);
		
		assertEquals(30, resultado);
	}

}
