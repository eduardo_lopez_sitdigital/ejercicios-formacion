package com.formacion.ejercicios.spring.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.formacion.ejercicios.spring.utils.Matematicas;

@Component
public class Pagos {

	private List<Long> pagos;
	private long total;

	@Autowired
	private Matematicas mate;

	public Pagos() {
		this.pagos = new ArrayList<>(0);
	}


	public void agregarPagos(long... pagos) {

		long suma = 0;

		for (long pago : pagos) {
			this.pagos.add(pago);
			suma = mate.suma(suma, pago);
		}

		this.total += suma;

	}
	
	public List<Long> getPagos() {
		return pagos;
	}

	public void setPagos(List<Long> pagos) {
		this.pagos = pagos;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Pagos [pagos=");
		builder.append(pagos);
		builder.append(", total=");
		builder.append(total);
		builder.append(", mate=");
		builder.append(mate);
		builder.append("]");
		return builder.toString();
	}

}
