package com.formacion.ejercicios;

import com.formacion.ejercicios.inyecciondependencias.MatematicasBigImpl;
import com.formacion.ejercicios.inyecciondependencias.MatematicasImpl;
import com.formacion.ejercicios.inyecciondependencias.Pagos;

/**
 * Hello world!
 *
 */
public class App {
	
	
	public static void main(String[] args) {
		System.out.println("Hello World!");
		
		long[] pagos = new long[5];
		pagos[0] = 10;
		pagos[1] = 20;
		pagos[2] = 30;
		pagos[3] = 40;
		pagos[4] = 50;

		Pagos pagosEduardo = new Pagos(new MatematicasBigImpl());
		pagosEduardo.agregarPagos(pagos);
		
		System.out.println(pagosEduardo.toString());
		
		Pagos pagosJose = new Pagos(new MatematicasImpl());
		pagosJose.agregarPagos(5, 5, 9, 8, 6, 7, 5);
		
		System.out.println(pagosJose.toString());
		
		pagosEduardo.agregarPagos(pagos);
		System.out.println(pagosEduardo.toString());
	
	}
}
