package com.formacion.ejercicios.inyecciondependencias;

public interface Matematicas {
	
	long suma(long a, long b);
	
	long multiplicacion(long a, long b);


}
