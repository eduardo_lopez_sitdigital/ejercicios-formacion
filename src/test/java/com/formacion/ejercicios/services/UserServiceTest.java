package com.formacion.ejercicios.services;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.jeasy.random.EasyRandom;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.formacion.ejercicios.dao.UserDao;
import com.formacion.ejercicios.dao.UserDaoImpl;
import com.formacion.ejercicios.dto.UserDto;


public class UserServiceTest {
	
	 private UserDto user;
	 private UserDto userNew;
	 
	 @Before
	 public void before() {
		 user = new UserDto();
		 user.setAge(12);
		 user.setName("pedro");
		 user.setGender("M");
	 }
	
	
	 @Test
	 public void getUserById() {
		 
		 long id = 10;
		 
		 /*
		 EasyRandom generator = new EasyRandom();
		 UserDto userRandom = generator.nextObject(UserDto.class);
		 */
		 
		 //Generar un objeto
		 UserDto user = new UserDto();
		 user.setName("pedro");
		 user.setAge(30);
		 user.setId(id);
		 user.setGender("M");

		 //Mockito framework para Java
		 UserDao userDaoMock = mock(UserDao.class);
		 //prear un caso cuando existe una ejecución del método getUserById de la clase UserDao
		 when(userDaoMock.getUserById(10)).thenReturn(user);
		
		
		 //
		 UserService userService = new UserServiceImpl(new UserDaoImpl());
		 
		 //validar el método getUserById(10)
		 UserDto userResult = userService.getUserById(id);
		 
		 //validaciones
		 assertNotNull(userResult);
		//validaciones de los atributos del objeto
		 assertEquals(user.getId(),userResult.getId());
		 assertEquals(user.getName(),userResult.getName());
		 assertEquals(user.getAge(),userResult.getAge());
		 assertEquals("MASCULINO",userResult.getGender());
	 }
	 
	 
	 @After
	 public void a() {
	
	
	 }

}
