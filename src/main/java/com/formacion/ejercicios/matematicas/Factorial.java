package com.formacion.ejercicios.matematicas;

public class Factorial {
	
	public void main(String [] args) {
		// hicieron todo el código de factorial
		
		long result = factorial(7);
		//impresion
	}
	
	// un método que calcule el factorial
	public static long factorial(long a) {
		
		long factorial = 1;
		
		if(a == 0 || a == 1) {
			return factorial;
		}
				
		for(int i=1; i <= a ; i++) {
			factorial = factorial * i;
		}
		
		return factorial;
	}
	
	

}
