package com.formacion.ejercicios.matematicas;


//4. Pedir un número, comprobar si es primo y preguntar si quiere introducir otro número
public class Matematicas {
	
	
	public static boolean esNumeroPrimo(int numero) {
		if (numero < 2) {
			return false;
		}else {
			return esPrimo(numero);
		}
	}
	
	public static int suma(int a, int b) {
		return a+b;
	}
	
	
	private static boolean esPrimo (int num) {
		int cont, i;
		cont = 0;
		i = 0;
		for (i = 2; i <= num; i++) {
			if(num % i == 0 ) {
				cont++;
			}
		}
		
		if (cont >= 2) {
			return false;
		}else {
			return true;
		}
	}
	
	/*
	public static void main(String[] args) {
			// TODO Auto-generated method stub
			int num;
			boolean otro;
			Scanner sc = new Scanner(System.in);
			
			do {

				System.out.print("Introduce un número entero: ");
				num = sc.nextInt();
			
				if (num < 2) {
					System.out.println("El numero NO es primo");
				}else {
					if (esPrimo(num)) {
						System.out.println("El numero es primo");
					}else {
						System.out.println("El numero NO es primo");
					}
				}
				
				System.out.print("Desea introducir otro numero? (true/false) ");
				otro = sc.nextBoolean();
			
			} while (otro);
			
			if (!otro) {
				System.out.println("FIN");
			}
		}*/
		



}
