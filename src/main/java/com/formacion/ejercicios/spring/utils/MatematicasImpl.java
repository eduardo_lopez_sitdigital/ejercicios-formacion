package com.formacion.ejercicios.spring.utils;

import org.springframework.stereotype.Service;

@Service
public class MatematicasImpl implements Matematicas {
	

	@Override
	public long suma(long a, long b) {
		return a + b;
	}
	
	@Override
	public long multiplicacion(long a, long b) {
		return a * b;
	}

}
