package com.formacion.ejercicios.matematicas;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MatematicasTest {
	
	
	//Un test unitario
	@Test
	public void validarNumeroPrimo() {
		
		int numeroPrimo = 773;
		
		//invocando el método esNumeroPrimo de la clase Matematicas
		boolean resultado = Matematicas.esNumeroPrimo(numeroPrimo);
		
		//Revisando resultados, validar si el resultado es igual al dato esperado
		assertEquals(true, resultado);
		
		
	}
	
	//segundo caso de prueba
	@Test
	public void validarNumeroNoPrimo() {
		
		int numero = 10;
		
		//invocando el método esNumeroPrimo de la clase Matematicas
		boolean resultado = Matematicas.esNumeroPrimo(numero);
		
		//Revisando resultados
		assertEquals(false, resultado);
		
	}
	
	
	@Test
	public void validarSuma() {
		
		//
		int resultado = Matematicas.suma(10,11);
		
		//Revisando resultados
		assertEquals(21, resultado);
		
	}
	

}
