package com.formacion.ejercicios.matematicas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class FactorialTest {
	
	
	 @Test
	 public void validarFactorial() {
		 
		 long resultado  = Factorial.factorial(7);
		 assertEquals( 5040 , resultado);
		 
		 resultado  = Factorial.factorial(15);
		 assertEquals( 1307674368000l , resultado);
		 
		
		 resultado  = Factorial.factorial(1);
		 assertEquals( 1 , resultado);
		 
	 }
	 
	 @Test
	 public void validarFactorialCero() {
		 
		 long resultado  = Factorial.factorial(0);
		 assertEquals( 1 , resultado);
		 
	 }

}
