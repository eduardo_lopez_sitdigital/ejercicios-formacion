package com.formacion.ejercicios.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.formacion.ejercicios.spring")
public class AppConfig {

}
