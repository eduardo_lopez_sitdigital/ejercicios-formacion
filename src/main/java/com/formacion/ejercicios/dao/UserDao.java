package com.formacion.ejercicios.dao;

import com.formacion.ejercicios.dto.UserDto;

public interface UserDao {
	
	
	UserDto getUserById(long id);

}
