package com.formacion.ejercicios.pagos;

import com.formacion.ejercicios.config.AppConfig;

import com.formacion.ejercicios.spring.services.Pagos;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


//Configuración de Test para usar Spring context
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { AppConfig.class })
public class PagosSpringTest {

	@Autowired
	private Pagos pagos;
	
	@Test
	public void validarPagos() {
	
		pagos.agregarPagos(1,2,3,4,5,6,7,8);
		
		System.out.println("pagos : "+ pagos.getTotal());
		assertNotNull(pagos.getTotal());
		assertEquals(36, pagos.getTotal());
		
		
	}
}
