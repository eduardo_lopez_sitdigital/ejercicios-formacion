package com.formacion.ejercicios.inyecciondependencias;

public class MatematicasBigImpl implements Matematicas {

	@Override
	public long suma(long a, long b) {
		
		return Math.addExact(a, b);
	}

	@Override
	public long multiplicacion(long a, long b) {
		
		return Math.multiplyExact(a, b);
	}

	
	
}
