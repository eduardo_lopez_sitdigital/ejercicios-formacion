package com.formacion.ejercicios.services;

import com.formacion.ejercicios.dao.UserDao;
import com.formacion.ejercicios.dto.UserDto;

public interface UserService {
	
	
	UserDto getUserById(long id);

}
