package com.formacion.ejercicios.inyecciondependencias;

public class MatematicasImpl implements Matematicas {
	

	@Override
	public long suma(long a, long b) {
		return a + b;
	}
	
	@Override
	public long multiplicacion(long a, long b) {
		return a * b;
	}

}
