package com.formacion.ejercicios.inyecciondependencias;

import java.util.ArrayList;
import java.util.List;

public class Pagos {
	
	private List<Long> pagos;
	private long total;
	
	//atributo, "variable global"
	private Matematicas mate;
	
	public Pagos(Matematicas matematicas) {
		this.mate = matematicas;
		this.pagos = new ArrayList<>(0);
	}
	
	//uso de var args de java
	//public void agregarPagos(long a, long b, long c, long h, ........ long n ) {
	// agregarPagos( long [] pagos)
	public void agregarPagos(long ... pagos) {
		
		//Matematicas a = new MatematicasBigImpl();
		
		long suma = 0;
		
		for(int i=0; i<pagos.length; i++ ) {
			this.pagos.add(pagos[i]);
			suma =  mate.suma(suma, pagos[i]);
		}
		
		/*for each
		for(long pago:  pagos) {
			this.pagos.add(pago);
			suma = mate.suma(suma,pago);
		}*/	
	
		//this.total = this.total + suma;
		this.total += suma;
		
	}
	
	public void actualizarPagos(long a , long v) {
		
		//sin necesidad de estar instanciando un objeto
		mate.multiplicacion(a, v);
	}
	
	public void borrarPagos() {
		mate.suma(10, 20);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Pagos [pagos=");
		builder.append(pagos);
		builder.append(", total=");
		builder.append(total);
		builder.append(", mate=");
		builder.append(mate);
		builder.append("]");
		return builder.toString();
	}
	
	

}
